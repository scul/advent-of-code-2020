class VM:
    def __init__(self, pgm=None):
        self.pgm = pgm
        self.accumulator = 0
        self.ip = 0
        self.ip_seen = set()
        self._continue = True
        self.inst = {'jmp': self.__jmp,
                     'acc': self.__acc,
                     'nop': self.__nop}

    def setup(self, pgm):
        self.__init__(pgm)

    def __jmp(self, val):
        self.ip += int(val)

    def __acc(self, val):
        self.ip += 1
        self.accumulator += int(val)

    def __nop(self, val):
        self.ip += 1

    def step(self):
        if self.ip in self.ip_seen:  # Dont repeat yourself,
            self._continue = False   # starting infinite loop
            return

        if self.ip >= len(self.pgm):  # Attempting to execute line after last line
            self._continue = False    # Terminate condition
            return
        self.ip_seen.add(self.ip)

        inst, val = self.pgm[self.ip].split(' ')

        self.inst[inst](val)

    def run(self):
        while self._continue:
            self.step()
        return self.accumulator, self.ip == len(self.pgm)
