with open('06.in') as f:
    data = f.read()

groups = data.split('\n\n')
tot = 0
for g in groups:
    l_g = len(g.split('\n'))
    g = g.replace('\n', '')
    g_s = set(g)
    for c in g_s:
        if g.count(c) == l_g:
            tot += 1

print(tot)
