import itertools

with open('09.in') as f:
    data = f.read().split()

# data = '''35
# 20
# 15
# 25
# 47
# 40
# 62
# 55
# 65
# 95
# 102
# 117
# 150
# 182
# 127
# 219
# 299
# 277
# 309
# 576
# '''.split()

data = list(map(int, data))
preamble = 25

target = -1

for i in range(preamble, len(data)):
    p = itertools.permutations(data[i-preamble:i], 2)
    sums = [sum(c) for c in p]
    if data[i] not in sums:
        print(data[i])
        target = data[i]
        break

start = 0
end = 1

while True:
    s = sum(data[start:end])
    if s > target:
        start += 1
        end = start + 1
        continue
    if s == target:
        set_ = data[start:end]
        print(sum([min(set_), max(set_)]))
        break

    if data[start] > target:
        break

    end += 1
