import vm

with open('08.in') as f:
    data = f.read().split('\n')

computer = vm.VM()
res = ('foo', False)
line = -1
while not res[1]:
    new_data = data[:]
    for i, v in enumerate(new_data):
        if i <= line:
            continue
        line = i
        if v[:3] == 'nop':
            new_data[i] = f'jmp {v[4:]}'
            break
        elif v[:3] == 'jmp':
            new_data[i] = f'nop {v[4:]}'
            break
    computer.setup(new_data)
    res = computer.run()
print(f'final val = {res[0]}')