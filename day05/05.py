with open('05.in') as f:
    data = f.read().split('\n')

# data = ['BFFFBBFRRR',
#         'FFFBBBFRRR',
#         'BBFFBBFRLL']


def sort(pos, start, end):
    e = end - (end - start) // 2
    if pos in 'FL':
        return (start, e - 1)
    else:
        return (e, end)


assert sort('F', 0, 127) == (0, 63)
assert sort('B', 0, 63) == (32, 63)

m = -1
seat_ids = []
for bp in data:
    rows = (0, 127)
    cols = (0, 7)
    for row in bp[:7]:
        # print(rows)
        rows = sort(row, *rows)
    for col in bp[7:]:
        # print(cols)
        cols = sort(col, *cols)
    seat_id = rows[0] * 8 + cols[0]
    seat_ids.append(seat_id)

seat_ids.sort()
for i in range(seat_ids[0], seat_ids[-1]):
    if i not in seat_ids:
        print(i)
        break
