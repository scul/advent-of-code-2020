import re

with open('04.in') as f:
    data = f.read().split('\n\n')

passports = [d.replace('\n', ' ') for d in data]

passports = [{key: value for word in passport.split() for key, value in [word.split(':')]} for passport in passports]


def val_byr(v):
    return 1920 <= int(v) <= 2002


def val_iyr(v):
    return 2010 <= int(v) <= 2020


def val_eyr(v):
    return 2020 <= int(v) <= 2030


def val_hgt(v):
    if v[-2:] not in ['in', 'cm']:
        return False
    if v[-2:] == 'in':
        return 59 <= int(v[:-2]) <= 76
    elif v[-2:] == 'cm':
        return 150 <= int(v[:-2]) <= 193


def val_hcl(v):
    m = re.search(r'\#[0-9a-f]{6}', v)
    return m is not None


def val_ecl(v):
    return v in ['amb', 'blu', 'brn', 'gry', 'grn', 'hzl', 'oth']


def val_pid(v):
    m = re.search(r'^[0-9]{9}$', v)
    return m is not None


def val_cid(v):
    return True


validators = {'byr': val_byr,
              'iyr': val_iyr,
              'eyr': val_eyr,
              'hgt': val_hgt,
              'hcl': val_hcl,
              'ecl': val_ecl,
              'pid': val_pid,
              'cid': val_cid}


def validate(p):
    for field, func in validators.items():
        if field not in p.keys() and field != 'cid':
            return 0
        if field != 'cid':
            if not func(p[field]):
                return 0
    return 1


print(sum(validate(p) for p in passports))
