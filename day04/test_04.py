import unittest
import d04


class MyTestCase(unittest.TestCase):
    def test_val_byr(self):
        self.assertFalse(d04.val_byr('1919'))
        self.assertFalse(d04.val_byr('2003'))
        self.assertTrue(d04.val_byr('1920'))
        self.assertTrue(d04.val_byr('2002'))
        self.assertTrue(d04.val_byr('1986'))

    def test_val_iyr(self):
        self.assertFalse(d04.val_iyr('2009'))
        self.assertFalse(d04.val_iyr('2021'))
        self.assertTrue(d04.val_iyr('2010'))
        self.assertTrue(d04.val_iyr('2020'))
        self.assertTrue(d04.val_iyr('2015'))

    def test_val_eyr(self):
        self.assertFalse(d04.val_eyr('2019'))
        self.assertFalse(d04.val_eyr('2031'))
        self.assertTrue(d04.val_eyr('2020'))
        self.assertTrue(d04.val_eyr('2030'))
        self.assertTrue(d04.val_eyr('2025'))

    def test_val_hgt(self):
        self.assertFalse(d04.val_hgt('149cm'))
        self.assertFalse(d04.val_hgt('194cm'))
        self.assertFalse(d04.val_hgt('58in'))
        self.assertFalse(d04.val_hgt('77in'))
        self.assertFalse(d04.val_hgt('192'))
        self.assertFalse(d04.val_hgt('65'))
        self.assertTrue(d04.val_hgt('150cm'))
        self.assertTrue(d04.val_hgt('193cm'))
        self.assertTrue(d04.val_hgt('180cm'))
        self.assertTrue(d04.val_hgt('59in'))
        self.assertTrue(d04.val_hgt('76in'))
        self.assertTrue(d04.val_hgt('65in'))

    def test_val_hcl(self):
        self.assertFalse(d04.val_hcl('#01234g'))
        self.assertFalse(d04.val_hcl('a12345'))
        self.assertTrue(d04.val_hcl('#012345'))
        self.assertTrue(d04.val_hcl('#abcdef'))
        self.assertTrue(d04.val_hcl('#012def'))

    def test_val_ecl(self):
        self.assertFalse(d04.val_ecl('amd'))
        self.assertFalse(d04.val_ecl('blue'))
        self.assertTrue( d04.val_ecl('blu'))
        self.assertTrue( d04.val_ecl('amb'))
        self.assertTrue( d04.val_ecl('brn'))
        self.assertTrue( d04.val_ecl('gry'))
        self.assertTrue( d04.val_ecl('grn'))
        self.assertTrue( d04.val_ecl('hzl'))
        self.assertTrue( d04.val_ecl('oth'))

    def test_val_pid(self):
        self.assertFalse(d04.val_pid('00000001'))
        self.assertFalse(d04.val_pid('0123456789'))
        self.assertTrue(d04.val_pid('000000001'))

    def test_val_cid(self):
        self.assertTrue(d04.val_cid('foo'))

if __name__ == '__main__':
    unittest.main()
