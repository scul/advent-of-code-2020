import re

with open('02.in') as f:
    data = f.read().split('\n')

r = re.compile(r"^(\d+)-(\d+) ([a-z]): ([a-z]+)$")


def validate(s):
    m = r.match(s)
    if not m:
        return 0
    p1 = int(m.group(1)) - 1
    p2 = int(m.group(2)) - 1
    char = m.group(3)
    pwd = m.group(4)
    return (pwd[p1] == char and pwd[p2] != char) or\
           (pwd[p1] != char and pwd[p2] == char)


print(sum(validate(policy) for policy in data))
