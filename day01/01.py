import sys

with open('01.in') as f:
    data = list(map(int, f.read().split('\n')))

for i in data:
    for j in data:
        for k in data:
            if i + j +k == 2020:
                print(i * j * k)
                sys.exit()