with open('03.in') as f:
    data = f.read().split('\n')


def slope(r, d):
    pos = [0, 0]
    trees = 0
    while True:
        line = data[pos[1]]
        # print(line)
        pos[0] %= len(line)
        if line[pos[0]] == '#':
            trees += 1
        pos[0] += r
        pos[1] += d
        if pos[1] >= len(data):
           break

    return trees


slopes = [(1, 1), (3, 1), (5, 1), (7, 1), (1, 2)]
r = 1
for s in slopes:
    t = slope(*s)
    print(t)
    r *= t

print(r)