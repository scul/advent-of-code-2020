import os
import requests
import sys
from datetime import datetime


YEAR = 2020
day = datetime.utcnow().day

if len(sys.argv) > 1:
    day = sys.argv[1]
    print(day)

URL = f'https://adventofcode.com/{YEAR}/day/{day}/input'

dir_name = f'day{day:0>2}'
cookies = {}

with open('.session.cookie') as f:
    cookies['session'] = f.read().strip()

target_file = os.path.join(dir_name, f'{day:0>2}.in')
print(target_file)

with open(target_file, 'w') as f:
    req = requests.get(URL, cookies=cookies)
    f.write(req.text)
