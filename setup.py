import os

for day in range(8, 26):
    day = f'{day:0>2.0f}'
    os.mkdir(f'day{day}')
    with open(f'day{day}/d{day}.py', 'w') as f:
        f.write(f"with open('{day}.in') as f:\n    data = f.readline()")
    with open(f'day{day}/{day}.in', 'w') as f:
        f.write('')
    print(day)
